package postgres

import (
	"database/sql"
	"errors"

	"github.com/jmoiron/sqlx"
	"gitlab.com/blog_najot_talim/medium_user_service/storage/repo"
)

type permissionRepo struct {
	db *sqlx.DB
}

func NewPermission(db *sqlx.DB) repo.PermissionStorageI {
	return &permissionRepo{
		db: db,
	}
}

func (pr *permissionRepo) CheckPermission(userType, resource, action string) (bool, error) {
	query := `
		select id from permissions
		where user_type = $1 and resource = $2 and action = $3
`
	var id int64
	err := pr.db.QueryRow(query, userType, resource, action).Scan(&id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return false, err
		}
		return false, err
	}

	return true, nil
}
